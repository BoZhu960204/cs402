#include <stdio.h>
int main()
{
	int x;
	int y;
	printf("Please enter the First Number:");
	scanf_s("%d",&x);
	while (x<0) 
	{
		printf("the First Number shouldn't be non-negative,Please enter again:");
		scanf_s("%d", &x);
	}
	printf("Please enter the Second Number:");
	scanf_s("%d",&y);
	while (y < 0)
	{
		printf("the Second Number shouldn't be non-negative,Please enter again:");
		scanf_s("%d", &y);
	}
	printf("The result of Ackerman Function is:%d\n", Ackermann(x, y));
	return 0;
}

int Ackermann(int x, int y)
{
	if (x == 0)
	{
		return y + 1;
	}
	else if (y == 0)
	{
		return Ackermann(x - 1, 1);
	}
	else
	{
		return Ackermann(x - 1, Ackermann(x, y - 1));
	}
}