.data
	msg1: .asciiz "Please enter the first number:"
	msg2: .asciiz "Please enter the second number:"
	msg3: .asciiz "The first number should be non-negative,Please enter again:"
	msg4: .asciiz "The second number should be non-negative,Please enter again:"
	msg5: .asciiz "The Ackerman Function Result is:"
	
.text
.globl main
main:	
#Ask user for the first number 
	li $v0,4
	la $a0,msg1
	syscall
	
	li $v0,5 
	syscall
	beq $0,$0,TEST1
#Test the number to be non-negative,otherwise ask user to input again
LOOP1:
	li $v0,4
	la $a0,msg3
	syscall
	
	li $v0,5 
	syscall
TEST1:
# if the number is negative ask for another number
	slt $t0,$v0,$0
	bne $t0,$0,LOOP1	
	addu $t1,$v0,$0
#Ask user for the sencond number
	li $v0,4
	la $a0,msg2
	syscall
	
	li $v0,5 
	syscall
	beq $0,$0,TEST2
#Test the number to be non-negative,otherwise ask user to input again
LOOP2:
	li $v0,4
	la $a0,msg3
	syscall
	
	li $v0,5 
	syscall
TEST2:
# if the number is negative ask for another number
	slt $t0,$v0,$0
	bne $t0,$0,LOOP2	
	addu $t2,$v0,$0
# pass the paramter to the procedure
	move $a0,$t1
	move $a1,$t2
	
	sub $sp,$sp,4
	sw $ra,4($sp)
	# move $s0, $ra	# must save $ra since I’ll have a call
	jal Ackerman	# call ‘test’ with no parameters
	# move $ra, $s0	# restore the return address in $ra
	lw $ra,4($sp)
	add $sp,$sp,4
	
	move $t0,$v0
# show the 	
	li $v0,4
	la $a0,msg5
	syscall
	
	li $v0,1
	move $a0,$t0
	syscall
	
	jr $ra		# return from main

	
Ackerman:
	subu $sp, $sp, 4
	sw $ra, 4($sp)		# save the return address on stack
	
	beqz $a0, terminate	# test for termination
	beqz $a1, situation2	# test for termination
	
	subu $sp, $sp, 4	# do not terminate yet
	sw $a0, 4($sp)		# save the parameter
	sub $a1,$a1,1
	jal Ackerman
# after the termination condition is reached these lines
# will be executed
	lw $a0, 4($sp)		# the argument I have saved on stack
	sub $a0, $a0, 1		# will call with a smaller argument
	addi $a1, $v0,0 	# do the multiplication
	jal Ackerman
	lw $ra, 8($sp)		# prepare to return
	addu $sp, $sp, 8	# I’ve popped 2 words (an address and
	jr $ra			# .. an argument)
terminate:
	addi $a1,$a1,1
	move $v0,$a1		# 0! = 1 is the return value
	lw $ra, 4($sp)		# get the return address
	addu $sp, $sp, 4	# adjust the stack pointer
	jr $ra			# return

situation2:
	sub $a0,$a0,1		
	li $a1,1
	jal Ackerman
	lw $ra, 4($sp)		# get the return address
	addu $sp, $sp, 4	# adjust the stack pointer
	jr $ra			# return