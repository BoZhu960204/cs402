.text
.globl main
main:	
	sub $sp,$sp,4	# subtract stack pointer by 4
	sw $ra,4($sp)	# save the address the main have to return on stack
	# move $s0, $ra	# must save $ra since I’ll have a call
	jal test	# call ‘test’ with no parameters
	nop		# execute this after ‘test’ returns
	# move $ra, $s0	# restore the return address in $ra
	lw $ra,4($sp)  	# restore the address the main have to return to $ra
	add $sp,$sp,4	# reset stack pointer point to the origin address
	jr $ra		# return from main
# The procedure ‘test’ does not call any other procedure. Therefore $ra
# does not need to be saved. Since ‘test’ uses no registers there is
# no need to save any registers.
test:	nop		# this is the procedure named ‘test’
	jr $ra		# return from this procedure