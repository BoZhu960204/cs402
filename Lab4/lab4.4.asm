.data 
	msg1: .asciiz "Please enter the first numbers:"
	msg2: .asciiz "Please enter the second numbers:"
	msg3: .asciiz "The largest number of the two numbers is:"
.text
.globl main
main:	
	# ask for the first number
	li $v0,4
	la $a0,msg1
	syscall
	
	# read the input from the user
	li $v0,5 
	syscall
	addu $t0,$v0,$0
	
	# ask for the second number
	li $v0,4
	la $a0,msg2
	syscall
	
	# read the input from the user
	li $v0,5
	syscall
	addu $t1,$v0,$0
	
	# store the address to return and pass the parameter by stack
	sub $sp,$sp,12
	sw $ra,12($sp)
	sw $t0,8($sp)
	sw $t1,4($sp)
	# move $s0, $ra	# must save $ra since I’ll have a call
	jal Largest	# call ‘test’ with no parameters
	# move $ra, $s0	# restore the return address in $ra
	lw $ra,12($sp)
	add $sp,$sp,12
	
	move $t0,$v0
	
	# show the result of the function
	li $v0,4
	la $a0,msg3
	syscall
	
	li $v0,1
	move $a0,$t0
	syscall
	
	jr $ra		# return from main
# The procedure ‘test’ does not call any other procedure. Therefore $ra
# does not need to be saved. Since ‘test’ uses no registers there is
# no need to save any registers.
Largest:	
		lw $a0,8($sp)
		lw $a1,4($sp)
		slt $t0,$a1,$a0
		beq $t0,$0,Branch1
		move $v0,$a0
		jr $ra		# return from this procedure
Branch1:
		move $v0,$a1
		jr $ra
		