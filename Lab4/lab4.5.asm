.data
	msg1: .asciiz "Please enter a number:"
	msg2: .asciiz "The Factorial Result is:"
	msg3: .asciiz "The number should be non-negative,Please enter again:"
.text
.globl main
main:	
	# ask for the number
	li $v0,4
	la $a0,msg1
	syscall
	
	li $v0,5 
	syscall
	# read the number and go to test
	beq $0,$0,TEST
LOOP:
	li $v0,4
	la $a0,msg3
	syscall
	li $v0,5 
	syscall
TEST:
	# if the number is negative ask for another number
	slt $t0,$v0,$0
	bne $t0,$0,LOOP	
	addu $a0,$v0,$0
	
	sub $sp,$sp,4
	sw $ra,4($sp)
	# move $s0, $ra	# must save $ra since I’ll have a call
	jal Factorial	# call ‘test’ with no parameters
	# move $ra, $s0	# restore the return address in $ra
	lw $ra,4($sp)
	add $sp,$sp,4
	
	move $t0,$v0
	
	li $v0,4
	la $a0,msg2
	syscall
	
	li $v0,1
	move $a0,$t0
	syscall
	
	jr $ra		# return from main

	
# This procedure computes the factorial of a non-negative integer
# The parameter (an integer) received in $a0
# The result (a 32 bit integer) is returned in $v0
# The procedure uses none of the registers $s0 - $s7 so no need to save them
# Any parameter that will make the factorial compute a result larger than
# 32 bits will return a wrong result.
Factorial:
	subu $sp, $sp, 4
	sw $ra, 4($sp)		# save the return address on stack
	
	beqz $a0, terminate	# test for termination
	subu $sp, $sp, 4	# do not terminate yet
	sw $a0, 4($sp)		# save the parameter
	sub $a0, $a0, 1		# will call with a smaller argument
	jal Factorial
# after the termination condition is reached these lines
# will be executed
	lw $t0, 4($sp)		# the argument I have saved on stack
	mul $v0, $v0, $t0	# do the multiplication
	lw $ra, 8($sp)		# prepare to return
	addu $sp, $sp, 8	# I’ve popped 2 words (an address and
	jr $ra			# .. an argument)
terminate:
	li $v0, 1		# 0! = 1 is the return value
	lw $ra, 4($sp)		# get the return address
	addu $sp, $sp, 4	# adjust the stack pointer
	jr $ra			# return