#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#include "readfile.h"

#define MAXNAME  64     /* #define and use constants for values that don't change */

/**
 * Date 2020/4/5
 * Author Bo Zhu
 * This is a menu interactive window program written in C
 * User read in a database file into the application and choose what he wants to do with the data
 * Program will perform the operations and return the operation result.
 * User can quit the program when all his operations are done.
 */

typedef struct PersonStruct {
	int six_digit_ID;
	char first_name[MAXNAME];   /* a C-style string (array of chars) */
	char last_name[MAXNAME];
	int  salary;
}Person;

Person employeeArray[1024];

extern FILE* fp;
/**
 *	Program main enterance	
 */
int main(int argc, char** argv) {

	int lastIndex = 0;

	// if user didn't enter the file containing the data,exit the program
	if (argc < 2)
	{
		printf("Please specify the path of the file containing employee information when execute the program.\n");
		return EXIT_SUCCESS;
	}

	// success open the file
	if (open_file(argv[1]) == 0)
	{
		int six_digit_ID;
		char first_name[MAXNAME];
		char last_name[MAXNAME];
		int salary;

		
		// read the data into the array
		while (!feof(fp))
		{
			read_int(&six_digit_ID);
			read_string(first_name);
			read_string(last_name);
			read_int(&salary);

			Person *p4 = (Person*)malloc(sizeof(Person));
			strcpy(p4->first_name, first_name);
			strcpy(p4->last_name, last_name);
			p4->salary = salary;
			p4->six_digit_ID = six_digit_ID;

			sortInsertElement(employeeArray, *p4);
			lastIndex++;
		}

		close_file();
	}
	// exit the program
	else
	{
		printf("Invalid file,please specify a valud file!");
		return;
	}

	while (1)
	{
		// print the menu
		printf("\n");
		printf("Employee DB Menu:\n");
		printf("----------------------------------\n");
		printf("  (1) Print the Database\n");
		printf("  (2) Lookup by ID\n");
		printf("  (3) Lookup by Last Name\n");
		printf("  (4) Add an Employee\n");
		printf("  (5) Quit\n");
		printf("----------------------------------\n");
		printf("Enter your choice :");

		// save user's choice
		char choice[20];
		// save the DigitId user want to search
		signed int searchDigitId;
		// save the lastName user want to search
		char searchLastName[64];

		// save the first name of the employee user wants to add
		char newEmployeeFirstName[64];
		// save the lastname of the employee user wants to add
		char newEmployeeLastName[64];
		// save the salary of the employee user wants to add
		signed int newEmployeeSalary;
		// save the digitNumber of the employee to add
		signed int newEmployeeDigitNumber;

		int flag = 0;

		int i;


		scanf("%s", &choice);
		//ask user for a valid input
		while (strlen(choice) > 1 || (choice[0] != '1'&&choice[0] != '2'&&choice[0] != '3'&&choice[0] != '4'&&choice[0] != '5'))
		{
			printf("Your input must be a number between 1-5,Enter again!\n");
			printf("Enter your choice :");
			scanf("%s", &choice);
		}

		switch (choice[0])
		{
		case '1':
			//print employee list
			printf("\n");
			printf("%-30s%-20s%-20s\n", "NAME", "SALARY", "ID");
			printf("---------------------------------------------------------------\n");
			for (i = 0; employeeArray[i].six_digit_ID != NULL; i++)
			{
				Person p = employeeArray[i];
				printf("%-10s%-20s%-20d%-20d\n", p.first_name, p.last_name, p.salary, p.six_digit_ID);
			}
			printf("---------------------------------------------------------------\n");
			printf(" Number of Employees (%d)\n", i);
			break;
		case '2':
			printf("\n");
			printf("Enter a 6 digit employee id:");
			scanf("%d", &searchDigitId);
			// binary search for the employee in the array
			if (binarySearchEmployee(employeeArray, 0, lastIndex, searchDigitId) == -1)
			{
				printf("Employee with id %d not found in DB\n", searchDigitId);
			}
			
			break;
		case '3':
			printf("Enter Employee's last name (no extra spaces):");
			scanf("%s", searchLastName);
			// iterate array and search for the employee with the last name
			for (int i = 0; employeeArray[i].six_digit_ID != NULL; i++)
			{
				if (strcmp(employeeArray[i].last_name, searchLastName) == 0)
				{
					Person p = employeeArray[i];
					printf("\n");
					printf("%-30s%-20s%-20s\n", "NAME", "SALARY", "ID");
					printf("---------------------------------------------------------------\n");
					printf("%-10s%-20s%-20d%-20d\n", p.first_name, p.last_name, p.salary, p.six_digit_ID);
					printf("---------------------------------------------------------------\n");
					// find
					flag = 1;
					break;
				}
			}
			if (!flag)
			{
				printf("Employee with last name %s not found in DB\n", searchLastName);
			}
			// reset flag to 0
			flag = 0;
			break;
		case '4':
			//ask for user to input the valid attribute of the employee
			printf("Enter the first name of the employee:");
			scanf("%s", newEmployeeFirstName);
			printf("Enter the last name of the employee:");
			scanf("%s", newEmployeeLastName);
			printf("Enter employee's salary (30000 to 150000):");
			scanf("%d", &newEmployeeSalary);
			while (newEmployeeSalary < 30000 || newEmployeeSalary>150000)
			{
				printf("Employee's salary must be a number between 30000-150000,Enter again!\n");
				printf("Enter employee's salary (30000 to 150000):");
				scanf("%d", &newEmployeeSalary);
			}

			// generate the user's digitNumber
			for (int i = 1023;; i--)
			{
				if (employeeArray[i].six_digit_ID != NULL)
				{
					newEmployeeDigitNumber = employeeArray[i].six_digit_ID + 1;
					break;
				}
			}

			// create a person with the input of the user
			Person *p4 = (Person*)malloc(sizeof(Person));
			strcpy(p4->first_name, newEmployeeFirstName);
			strcpy(p4->last_name, newEmployeeLastName);
			p4->salary = newEmployeeSalary;
			p4->six_digit_ID = newEmployeeDigitNumber;

			// ask user if he wants to add the employee into the array
			printf("do you want to add the following employee to the DB?\n");
			printf("%20s %s,salary: %-20d\n", newEmployeeFirstName, newEmployeeLastName, newEmployeeSalary);
			printf("Enter 1 for yes, 0 for no:");
			scanf("%s", &choice);

			// ask for a valid input
			while (strlen(choice) > 1 || (choice[0] != '1'&&choice[0] != '0'))
			{
				printf("Your input must be a number between 0-1,Enter again!\n");
				printf("Enter your choice :");
				scanf("%s", &choice);
			}
			// if choice equal to '1',add the user into the array
			if (choice[0] == '1')
			{
				for (int i = 0;; i++)
				{
					if (employeeArray[i].six_digit_ID == NULL)
					{
						employeeArray[i] = *p4;
						break;
					}
				}
			}
			break;
		case '5':
			printf("goodbye!");
			return EXIT_SUCCESS;
		}
	}
	system("pause");
	return EXIT_SUCCESS;
}

// method to add the employee into the array in sort order(asc)
int sortInsertElement(Person* employeeArray, Person p)
{
	int len = 1024;

	if (!employeeArray[len - 1].six_digit_ID == NULL)
	{
		printf("array is full");
		return -1;
	}

	for (int i = 0; i < len; i++)
	{
		if (p.six_digit_ID < employeeArray[i].six_digit_ID)
		{
			for (int j = len - 1; j > i; j--)
			{
				employeeArray[j] = employeeArray[j - 1];
			}
			employeeArray[i] = p;
			return 0;
		}
		else if (employeeArray[i].six_digit_ID == NULL)
		{
			employeeArray[i] = p;
			return 0;
		}
	}
	return 0;
}

// search for the employee with the digitID in the array
int binarySearchEmployee(Person* employeeArray, int fromIndex, int endIndex, int six_digit_ID)
{
	int mid = (fromIndex + endIndex) / 2;
	if (fromIndex > endIndex || (fromIndex == endIndex && employeeArray[mid].six_digit_ID != six_digit_ID))
	{
		return -1;
	}
	if (employeeArray[mid].six_digit_ID == six_digit_ID)
	{
		Person p = employeeArray[mid];
		printf("\n");
		printf("%-30s%-20s%-20s\n", "NAME", "SALARY", "ID");
		printf("---------------------------------------------------------------\n");
		printf("%-10s%-20s%-20d%-20d\n", p.first_name, p.last_name, p.salary, p.six_digit_ID);
		printf("---------------------------------------------------------------\n");
		return mid;
	}
	else if (employeeArray[mid].six_digit_ID < six_digit_ID)
	{
		return binarySearchEmployee(employeeArray, mid + 1, endIndex, six_digit_ID);
	}
	else
	{
		return binarySearchEmployee(employeeArray, fromIndex, mid, six_digit_ID);
	}
}