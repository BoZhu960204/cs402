#include <stdio.h>
#define  _CRT_SECURE_NO_WARNINGS
FILE* fp;

int open_file(char* path) 
{
	fp = fopen(path, "r");
	if (fp == NULL) 
	{
		printf("file open failed!");
	}
	return 0;
}

int read_int(int* a)
{
	fscanf(fp,"%d", a);
	return 0;
}
int read_string(char* str)
{
	fscanf(fp,"%s", str);
	return 0;
}
int read_float(float* a)
{
	fscanf(fp,"%f", a);
	return 0;
}


void close_file() 
{
	if (fp != NULL)
	{
		fclose(fp);
		fp = NULL;
	}
}

