#Descirbe

You can build it in Visual Studio or Visual Studio Code.
readfile.h and readfile.c are the lib file relate to read data from file.
workDB.c is the source code for the executable
----------------------------------------------------------------
#Deployment

Copy all the files including .vscode directory into your VS Code,
then you can compile it with your VS Code.
----------------------------------------------------------------
#Local Compie Environment

gcc version 6.3.0 (MinGW.org GCC-6.3.0-1)
----------------------------------------------------------------
#Execute

Or you can compile it with the command line:
gcc workDB.c readfile.c -o workDB

you can run the executable with command line:
./workDB employee.txt
---------------------------------------------------------------
#Code

There is three part of the Code
1.main function
    to run the main program
2.custom define function
    for reuse in the main program 
    define in the bottom of workDB.c 
3.readfile library
    for reading data from local text file