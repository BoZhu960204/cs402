int open_file(char* path);
int read_int(int* a);
int read_string(char* str);
int read_float(float* a);
void close_file();