.data
	word1: 		.word 0x89abcdef
.text
.globl main
main:	
	la $s0,word1
	# use lwl instruction to load word1 into register
	lwl $t0,0($s0)
	lwl $t1,1($s0)
	lwl $t2,2($s0)
	lwl $t3,3($s0)
	
	jr $ra		# return from main
# The procedure ‘test’ does not call any other procedure. Therefore $ra
# does not need to be saved. Since ‘test’ uses no registers there is
# no need to save any registers.