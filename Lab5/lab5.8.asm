.data
	word1: 		.word 0x89abcdef
.text
.globl main
main:	
	la $s0,word1
	# use lwr instruction to load word1 into register
	lwr $t4,0($s0)
	lwr $t5,1($s0)
	lwr $t6,2($s0)
	lwr $t7,3($s0)
	
	jr $ra		# return from main
# The procedure ‘test’ does not call any other procedure. Therefore $ra
# does not need to be saved. Since ‘test’ uses no registers there is
# no need to save any registers.