# this is a program used to test memory alignment for data
.data 0x10000000
.align 0
ch1: 	.byte 	'a'
word1: 	.word	0x89abcdef
ch2: 	.byte	'b'
word2: 	.word	0

.text
.globl main
main:	
	# save value in word1 into word2
	lui $1, 4096             
	lw $t1, 4($1)             
	lui $1, 4096              
	sw $t1, 12($1) 
	
	jr $ra			# return from main