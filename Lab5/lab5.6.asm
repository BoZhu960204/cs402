.data
	msg1: 		.asciiz "Please enter an number:"
	msg2:		.asciiz "If bytes were layed in reverse order the number would be:"
	user1: 		.word 0 
.text
.globl main
main:	
	sub $sp,$sp,4	# subtract stack pointer by 4
	sw $ra,4($sp)	# save the address the main have to return on stack
	li $v0,4
	la $a0,msg1
	syscall
	
	li $v0,5
	syscall
	sw $v0,user1
	
	la $a0,user1
	#  $s0, $ra	# must save $ra since I’ll have a call
	jal Reverse_bytes	# call ‘test’ with no parameters
	li $v0,4
	la $a0,msg1
	syscall
	
	lw $a0,user1
	li $v0,1
	syscall
	
	# move $ra, $s0	# restore the return address in $ra
	lw $ra,4($sp)  	# restore the address the main have to return to $ra
	add $sp,$sp,4	# reset stack pointer point to the origin address
	jr $ra		# return from main
# The procedure ‘test’ does not call any other procedure. Therefore $ra
# does not need to be saved. Since ‘test’ uses no registers there is
# no need to save any registers.
Reverse_bytes:
	lbu $t0,0($a0)
	lbu $t1,1($a0)
	lbu $t2,2($a0)
	lbu $t3,3($a0)
	
	sb $t0,3($a0)
	sb $t1,2($a0)
	sb $t2,1($a0)
	sb $t3,0($a0)
	jr $ra		# return from this procedure