import time
import numpy as np
print("—————————Version1———————————")
# use total to record total time
total = 0
# run 5 times
for i in range(5):
    # record start time
    start = time.process_time()
    # create a random 1200*800 Integer matrix and a random 800*1200 Integer matrix
    a = np.random.randint(1000,size =(1200, 800))
    b = np.random.randint(1000,size=(800, 1200))
    # dot them
    c = np.dot(a, b)
    # record time cost
    cost = time.process_time() - start
    # add cost to the total time
    total += cost

print("Integer Matrix Version Average",total/5)

# clear total
total = 0
print("—————————Version2———————————")
# run 5 times
for i in range(5):
    # record start time
    start = time.process_time()
    # create a random 1700*1800 Double matrix and a random 1800*1700 Double matrix
    a = np.random.rand(1800, 1700)
    b = np.random.rand(1700, 1800)
    # dot them
    c = np.dot(a, b)
    # record time cost
    cost = time.process_time() - start
    # add cost to the total time
    total += cost

print("Double Matrix Version Average",total/5)

#===============Change the Algorithm=================================
print("—————————Version1———————————")
# use total to record total time
total = 0
# run 5 times
for i in range(5):
    # record start time
    start = time.process_time()
    # create a random 1400*700 Integer matrix and a random 700*1400 Integer matrix
    # change the range here and change the size
    a = np.random.randint(2000,size =(1400, 700))
    b = np.random.randint(2000,size=(700, 1400))
    # dot them
    c = np.dot(a, b)
    # record time cost
    cost = time.process_time() - start
    # add cost to the total time
    total += cost

print("Integer Matrix Version Average",total/5)

# clear total
total = 0
print("—————————Version2———————————")
# run 5 times
for i in range(5):
    # record start time
    start = time.process_time()
    # create a random 1700*1800 Double matrix and a random 1800*1700 Double matrix
    # change the size here
    a = np.random.rand(2500, 1200)
    b = np.random.rand(1200, 2500)
    # dot them
    c = np.dot(a, b)
    # record time cost
    cost = time.process_time() - start
    # add cost to the total time
    total += cost

print("Double Matrix Version Average",total/5)


