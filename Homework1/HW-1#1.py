import pandas as pd
from pandas import DataFrame
from matplotlib import pyplot as plt

#——————HomeWork1————————————————(a)————————————————
# read data from both file save to data,data2
data = pd.read_csv("./spice.din",sep=" ",header=None,names=['Operation','Address'])
data2 = pd.read_csv("./cc1.din",sep=" ",header=None,names=['Operation','Address'])

# create DataFrame with data of both Operation column combine with Address Column in decimal format
# transform Address Column from Hex to Decimal
df1 = DataFrame({'Operation':data.Operation,'Address':data['Address'].apply(lambda x: int(x,16))})
df2 = DataFrame({'Operation':data2.Operation,'Address':data2['Address'].apply(lambda x: int(x,16))})


# create first subplot
plt.subplot(1,2,1)
# use Address of spice as X axis of the first subplot
plt.hist(x = df1['Address'],color = 'steelblue',edgecolor = 'black')
# comment on the histogram
plt.xlabel("address")
plt.ylabel("occurrences")
plt.title("Histogram of spice.address")

# create second subplot
plt.subplot(1,2,2)
# use Address of cc1 as X axis of the second subplot
plt.hist(x = df2['Address'],color = 'blue',edgecolor = 'black')
# comment on the histogram
plt.xlabel("address")
plt.ylabel("occurrences")
plt.title("Histogram of cc1.address")

# show the graph
plt.show()

#——————HomeWork1————————————————(b)————————————————
# use the number of the Write Operation divide the sum Operation Number in the space.din file
print("write frequency in spice.din: %.2f%%" % (df1['Operation'].value_counts()[1]*100/df1['Operation'].count()))
# use the number of the Read Operation divide the sum Operation Number in the space.din file
print("read frequency in spice.din: %.2f%%" % (df1['Operation'].value_counts()[0]*100/df1['Operation'].count()))
print("————————————————————————————————————————————————————————")
# use the number of the Write Operation divide the sum Operation Number in the cc1.din file
print("write frequency in cc1.din: %.2f%%" % (df2['Operation'].value_counts()[1]*100/df2['Operation'].count()))
# use the number of the Read Operation divide the sum Operation Number in the cc1.din file
print("read frequency in cc1.din: %.2f%%" % (df2['Operation'].value_counts()[0]*100/df2['Operation'].count()))

