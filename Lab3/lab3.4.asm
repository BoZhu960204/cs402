.data 0x10010000
	my_array: .space 40	# deckare a space for my_array space 40
.text
.globl main	
	main: 
	li $t0,3	# load content of var1 to register t0
	la $t4,my_array # load the address of array to $t4
	addi $a0,$t4,40 # set value of a0 to the end of the array
	
Loop:	
	slt $t2,$t4,$a0 
	beq $t2,0,Exit  # if t4 out of the range of the array then branch Exit
	
	sw $t0,0($t4) 	# save t0 into the array
	addi $t0,$t0,1 	# add 1 to $t0's value
	addi $t4,$t4,4 	# add 4 to $t4's value
	j Loop
	
Exit:
	# Code for Test
	#la $t4,my_array
	#addi $a1,$t4,40
	
	#Loop2:	
	#li $v0, 1	# system call for print_str
	#lw $a0,0($t4)
	
	#syscall
	
	#slt $t2,$t4,$a1
	#beq $t2,0,Exit2

	#addi $t4,$t4,4
	
	#j Loop2
	
	li $v0,10	# system call for exit
	syscall		# exit
