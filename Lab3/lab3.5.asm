.data 0x10010000
	msg1: .asciiz "I'm far away"
	msg2: .asciiz "I'm nearby"

.text
.globl main	
	main: 
			# now get an integer from the user
	li $v0, 5 	# system call for read_int
	syscall 	# the integer placed in $v0
	move $t1,$v0
	
	li $v0, 5	# system call for read_int
	syscall 	# the integer placed in $v0
	move $t2,$v0
	
	beq $t1,$t2,Far	
	
	li $v0, 4 	# system call for print_str
	la $a0, msg2 	# address of string to print
	syscall
	
	li $v0,10	# system call for exit
	syscall		# exit

Far:
	li $v0, 4 	# system call for print_str
	la $a0, msg1 	# address of string to print
	syscall
	
	li $v0,10	# system call for exit
	syscall		# exit


	
