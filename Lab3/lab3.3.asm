.data 0x10010000
	var1: .word 3	# var1 is a word (32 bit) with the initial value 3
.text
.globl main	
	main: 
	lw $t0,var1	# load content of var1 to register t0
	li $a0,100	# save constant 100 into a0
	
Loop:	
	move $t1,$t0	# move t0 to t1
	slt $t2,$t1,$a0 
	ble $t2,0,Exit 	# if t1 >= 100 branch Exit
	
	addi $t1,$t1,1  # add one to value of $t1 
	j Loop
Exit:
	# sw $t0,var1
	# Code for Test
	# li $v0, 1	# system call for print_str

	# lw $a0,var1
	# syscall

	li $v0,10	# system call for exit
	syscall		# exit
