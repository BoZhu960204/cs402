.data 0x10010000
	var1: .word 3	# var1 is a word (32 bit) with the initial value 3
	var2: .word 2	# var2 is a word (32 bit) with the initial value 2
	var3: .word 5	# var2 is a word (32 bit) with the initial value 5
.text
.globl main	
	main: 
	lw $t0,var1	# load content of var1 to register t0
	lw $t1,var2	# load content of var2 to register t1
	lw $t2,var3 	# load content of var3 to register t2
	
	la $t3,var1	# load address of var1 to register t3
	la $t4,var2 	# load address of var2 to register t4
	
	bne $t0,$t1,Else	# if value of t0 and t1 are different then branch Else
	sw $t2,0($t3)		# save value of var3 into t1 and t2
	sw $t2,0($t4)
	ble $0,$0 Exit		# branch exit
	
Else:	
	move $t5,$t0 	# swap var1 and var2
	move $t0,$t1
	move $t1,$t5
	sw $t0,0($t3)	# save value of var2 into var1
	sw $t1,0($t4) 	# save value of var1 into var2
	
Exit:
	# Code for Test
	# li $v0, 1	# system call for print_str

	# lw $a0, 0($t3)
	# syscall
	# lw $a0, 0($t4)
	# syscall

	li $v0,10	# system call for exit
	syscall		# exit
