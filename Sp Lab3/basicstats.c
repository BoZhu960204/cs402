#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>


int arraySize = 10;
int size = 0;
double sum = 0;
double mean;
double median;
double stddev;

int main(int argc, char** argv) {
	double* numberArray = (double *)malloc(arraySize *sizeof(double));
	memset(numberArray, 0, arraySize * sizeof(double));
	
	
	// if user didn't enter the file containing the data,exit the program
	if (argc < 2)
	{
		printf("Please specify the path of the file containing employee information when execute the program.\n");
		return EXIT_SUCCESS;
	}
	// char* address = "D:/Numbers.txt";

	FILE* fp = fopen(argv[1], "r");
	// success open the file
	if (fp)
	{
		int index = 0;
		// read the data into the array
		while (!feof(fp))
		{	
			double a = 0;
            // read data into local variable
			fscanf(fp, "%lf", &a);
            // if the array is full,then enlarge the array into a array of twice the size of the original array
			if (index>=arraySize)
			{
				enlargeArray(&numberArray);
			}
            // put data into the array
			numberArray[index] = a;
			index++;  
		}
        // after reading the data into the array close the file
		if (fp != NULL)
		{
			fclose(fp);
			fp = NULL;
		}
	}
	// create a sorted order array
	double* sortedNumberArray = (double *)malloc(arraySize * sizeof(double));
	memset(sortedNumberArray, 0, arraySize * sizeof(double));

    // insert elements into the sorted order array in sorted order
	for (int i = 0; i < arraySize; i++)
	{
		if (numberArray[i])
		{
            // calculate the size and the sum
			size++;
			sum += numberArray[i];
			sortInsertNumber(sortedNumberArray, numberArray[i]);
		}
	}

    // calcute the median number
	if (size%2==1)
	{
		median = sortedNumberArray[(size-1)/2];
	}
	else
	{
		median = (sortedNumberArray[size/2-1]+sortedNumberArray[size/2]) / 2;
	}

    // calculate the mean of the set
	mean = sum / size;

    // initial stddev
	stddev = 0;

    // calculate the stddev
	for (int i = 0; i < arraySize; i++)
	{
		if (numberArray[i])
		{
			stddev += (numberArray[i]-mean)*(numberArray[i] - mean)/size;
		}
	}

	stddev = sqrt(stddev);


    // show the result,save 3 digit decimals
	printf("Results:\n");
	printf("--------\n");
	printf("%-20s%d\n","Num values:", size);
	printf("%-20s%.3lf\n","mean:",mean);
	printf("%-20s%.3lf\n","median:",median);
	printf("%-20s%.3lf\n","stddev:",stddev);
	printf("Unused array capacity:\t%d\n",(arraySize-size));
	system("pause");
	return EXIT_SUCCESS;
}

/*int enlargeArray(int** numberArray) 
{
	int* tempArray = (int *)malloc(arraySize*sizeof(int));
	memset(tempArray, 0, arraySize * sizeof(int));

	for (int i = 0; i < arraySize; i++)
	{
		*(tempArray + i) = *(*numberArray + i);
	}
	
	*numberArray = malloc(arraySize*sizeof(int)*2);
	memset(*numberArray, 0, arraySize * sizeof(int) * 2);

	for (int i = 0; i < arraySize; i++)
	{
		*(*numberArray + i) = *(tempArray + i);
	}

	arraySize *= 2;

	free(tempArray);
}*/

int enlargeArray(double** numberArray)
{
	double* tempArray = (double *)malloc(arraySize * sizeof(double));
	memset(tempArray, 0, arraySize * sizeof(double));

	for (int i = 0; i < arraySize; i++)
	{
		tempArray[i] = (*numberArray)[i];
	}

	*numberArray = malloc(arraySize * sizeof(double) * 2);
	memset(*numberArray, 0, arraySize * sizeof(double) * 2);

	for (int i = 0; i < arraySize; i++)
	{
		(*numberArray)[i] = tempArray[i];
	}

	arraySize *= 2;

	free(tempArray);
	return 0;
}

/*int sortInsertNumber(double* numberArray,double number) 
{
	for (int i = 0; i < size; i++)
	{
		if (number < *(numberArray + i))
		{
			for(int j = size-1;j >i; j--)
			{
				*(numberArray + j) = *(numberArray + j - 1);
			}
			*(numberArray + i) = number;
			return 0;
		}
		else if (*(numberArray + i)==0)
		{
			*(numberArray + i) = number;
			return 0;
		}
		return -1;
	}
}*/


int sortInsertNumber(double* numberArray, double number)
{
	for (int i = 0; i < size; i++)
	{
		if (number < numberArray[i])
		{
			for (int j = size - 1;j > i; j--)
			{
				numberArray[j] = numberArray[j-1];
			}
			numberArray[i] = number;
			return 0;
		}
		else if (numberArray[i] == 0)
		{
			numberArray[i] = number;
			return 0;
		}
	}
	return -1;
}