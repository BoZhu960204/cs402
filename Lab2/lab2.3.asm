.data 0x10010000
	var1: .word 24	# var1 is a word (32 bit) with the initial value 24
	var2: .word 26	# var2 is a word (32 bit) with the initial value 26
	var3: .word 29	# var3 is a word (32 bit) with the initial value 29
	var4: .word 53	# var4 is a word (32 bit) with the initial value 53
	
	first:.byte 66	# first is a byte with the initial value 66
	last: .byte 90	# last is a byte with the initial value 90
	
.text
.globl main	
	main: 
	lui $8, 4097     	# load address of var1 to register t0    
	lui $1, 4097     	# load address of var2 to register t1    
	ori $9, $1, 4                                           
	lui $1, 4097     	# load address of var3 to register t2    
	ori $10, $1, 8                                         
	lui $1, 4097    	# load address of var4 to register t3    
	ori $11, $1, 12                                       
	lui $1, 4097           	# load content of var1 to register t4    
	lw $12, 0($1)                                                 
	lui $1, 4097           	# load content of var1 to register t5    
	lw $13, 4($1)                                                 
	lui $1, 4097           	# load content of var1 to register t6    
	lw $14, 8($1)                                                 
	lui $1, 4097           	# load content of var1 to register t7    
	lw $15, 12($1)           
	sw $15, 0($8) 		# store content of var4 to address of var1          
	sw $14, 0($9)  		# store content of var3 to address of var2          
	sw $13, 0($10) 		# store content of var2 to address of var3          
	sw $12, 0($11) 		# store content of var1 to address of var4
	
	# Code for Test

	#li $v0, 1	# system call for print_str

	#lw $a0, 0($t0)
	#syscall
	#lw $a0, 0($t1)
	#syscall
	#lw $a0, 0($t2)
	#syscall
	#lw $a0, 0($t3)
	#syscall
	          
	ori $2, $0, 10 # system call for exit          
	syscall	       # exit   