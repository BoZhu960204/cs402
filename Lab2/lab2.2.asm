.data 0x10010000
	var1: .word 24	# var1 is a word (32 bit) with the initial value 24
	var2: .word 26	# var2 is a word (32 bit) with the initial value 26
	var3: .word 29	# var3 is a word (32 bit) with the initial value 29
	var4: .word 53	# var4 is a word (32 bit) with the initial value 53
	
	first:.byte 66	# first is a byte with the initial value 66
	last: .byte 90	# last is a byte with the initial value 90
	
.text
.globl main	
	main: 
	la $t0,var1	# load address of var1 to register t0
	la $t1,var2	# load address of var2 to register t1
	la $t2,var3	# load address of var3 to register t2
	la $t3,var4	# load address of var4 to register t3

	lw $t4,var1	# load content of var1 to register t4
	lw $t5,var2	# load content of var1 to register t5
	lw $t6,var3	# load content of var1 to register t6
	lw $t7,var4	# load content of var1 to register t7

	sw $t7,0($t0)	# store content of var4 to address of var1
	sw $t6,0($t1)	# store content of var3 to address of var2
	sw $t5,0($t2)	# store content of var2 to address of var3
	sw $t4,0($t3)	# store content of var1 to address of var4
	
	# Code for Test

	#li $v0, 1	# system call for print_str

	# lw $a0, 0($t0)
	# syscall
	# lw $a0, 0($t1)
	# syscall
	# lw $a0, 0($t2)
	# syscall
	# lw $a0, 0($t3)
	# syscall

	li $v0,10	# system call for exit
	syscall		# exit